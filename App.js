import React, {Component} from 'react';
import { mapping, light as lightTheme } from '@eva-design/eva';
import { ApplicationProvider, Layout } from 'react-native-ui-kitten';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
} from 'react-native';

import {
  Router,
  Stack,
  Scene,
} from 'react-native-router-flux';

import InicioView from './src/inicioView/inicioView';
import LoginView from './src/loginView/loginView';

const App = () => (
  <ApplicationProvider
    mapping={mapping}
    theme={lightTheme}>
    <Router>
      <Stack key="root">
        <Scene key="start" component={InicioView} title="pantalla inicio" hideNavBar/>
        <Scene key="login" component={LoginView} title="Login" hideNavBar/>
      </Stack>
    </Router>
  </ApplicationProvider>
);

export default App;
