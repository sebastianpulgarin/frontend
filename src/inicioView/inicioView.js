import React, {Component} from 'react';

import {
  View,
  Image,
  ImageBackground,
} from 'react-native';

import {
  Layout,
  Button,
  Text,
} from 'react-native-ui-kitten';

import { Actions } from 'react-native-router-flux';
import { styles } from './styles';


export default class InicioView extends Component<Props> {
  constructor(props){
    super(props)

    this.state = {

    };
  }

  render(){
    return(
      <View style={styles.principalContainer}>
        <ImageBackground
          resizeMode={"cover"}
          style={ styles.containerImageBackground }
          source={require("../assets/casa.jpg")}>
          <View style={styles.containerOpacity}>
            <View style={styles.containerLogo}>
              <Image
                style={styles.image}
                source={require("../assets/logo.png")}/>
            </View>
            <View style={styles.containerButtons}>
              <Button
                Type={"basic"}
                status={"white"}
                textStyle={styles.labelButton}
                style={styles.button}>
                  Register
              </Button>
              <Button
                onPress={() => {Actions.login()}}
                Type={"basic"}
                status={"white"}
                textStyle={styles.labelButton}
                style={styles.button}>
                  Login
              </Button>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
