import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  principalContainer: {
    flex:1,
  },
  containerImageBackground: {
    width:'100%',
    height: '100%',
  },
  containerOpacity: {
    flex:1,
    backgroundColor:'#e62e1b',
    opacity:0.85,
    justifyContent:'center'
  },
  containerLogo: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  image: {
    height:200,
    width:200,
  },
  containerButtons: {
    flex:1,
    alignItems:'center',
  },
  button: {
    width:'80%',
    margin:10,
    backgroundColor:'#fff',
    height:60,
    borderRadius:50
  },
  labelButton: {
    color:'red',
    fontSize:17
  }
});
