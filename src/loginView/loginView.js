import React, {Component} from 'react';

import {
  View,
  Text,
} from 'react-native';

import {
  Layout,
  Button,
  Input,
} from 'react-native-ui-kitten';

import { styles } from './styles.js';

export default class LoginView extends Component<Props> {
  constructor(props){
    super(props);

    this.state = {

    };
  }

  render(){
    return(
      <View style={styles.principalContainer}>
        <View style={styles.flexOne}>
          <Text style={styles.title}>Login</Text>
          <Input
            labelStyle={styles.label}
            style={styles.input}
            label='Your email'/>
            <Input
              labelStyle={styles.label}
              style={styles.input}
              label='Password'/>
          <Button
            Type={"basic"}
            status={"white"}
            textStyle={styles.labelButton}
            style={styles.button}>
              Login
          </Button>
          <Text style={styles.textInfo}>cant login?<Text style={styles.colorTextInfo}> Forgot Password!</Text></Text>
        </View>
        <View style={styles.footer}>
          <Text style={styles.textInfo}>Dont have an account?<Text style={styles.colorTextInfo}> Register!</Text></Text>
        </View>
      </View>
    )
  }
}
