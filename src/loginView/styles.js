import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  principalContainer: {
    flex:1,
    backgroundColor:'#fff'
  },
  flexOne: {
    flex:1
  },
  title: {
    fontSize:30,
    color:'red',
    fontWeight:'bold',
    margin:30
  },
  labelInput: {
    color:'gray',
    fontSize:17,
    padding:5,
    fontWeight:'bold'
  },
  input: {
    width:'85%',
    alignSelf:'center',
    borderRadius:50,
    backgroundColor:'#f7f7f7'
  },
  labelButton: {
    color:'#fff',
    fontSize:17
  },
  button: {
    width:'85%',
    margin:30,
    backgroundColor:'red',
    height:70,
    borderRadius:50,
    alignSelf:'center'
  },
  textInfo: {
    textAlign:'center',
    color:'gray',
    fontWeight:'bold'
  },
  colorTextInfo: {
    color:'red'
  },
  footer: {
    height:70,
    borderTopWidth:0.5,
    borderTopColor:'gray',
    justifyContent:'center'
  }
})
